<?php

foreach($arResult['ITEMS'] as $oneItem){
    $list[] = ['data' => ["ID" => $oneItem['ID'],
                          "ID_USER" => $oneItem['UF_ADDRESS_USER_ID'],
                          "ADDRESS" => $oneItem['UF_ADDRESS'],
                          "IS_ACTIVE" => (!$oneItem['UF_ADDRESS_IS_ACTIVE']) ? 'Нет' : 'Да']];
}
$APPLICATION->IncludeComponent(
    'bitrix:main.ui.grid',
    '',
    [
    'GRID_ID' => 'report_list',
    'COLUMNS' => [
        ['id' => 'ID', 'name' => 'ID записи', 'sort' => 'ID', 'default' => true],
        ['id' => 'ID_USER', 'name' => 'ID пользователя', 'sort' => 'DATE', 'default' => true],
        ['id' => 'ADDRESS', 'name' => 'Адрес', 'sort' => 'AMOUNT', 'default' => true],
        ['id' => 'IS_ACTIVE', 'name' => 'Активность', 'sort' => 'PAYER_INN', 'default' => true],
    ],
    'ROWS' => $list,
    'SHOW_ROW_CHECKBOXES' => true,

    'AJAX_MODE' => 'Y',
    'AJAX_ID' => \CAjax::getComponentID('bitrix:main.ui.grid', '.default', ''),
    'PAGE_SIZES' => [
        ['NAME' => "5", 'VALUE' => '5'],
        ['NAME' => '10', 'VALUE' => '10'],
        ['NAME' => '20', 'VALUE' => '20'],
        ['NAME' => '50', 'VALUE' => '50'],
        ['NAME' => '100', 'VALUE' => '100']
    ],
    'AJAX_OPTION_JUMP'          => 'N',
    'SHOW_CHECK_ALL_CHECKBOXES' => true,
    'SHOW_ROW_ACTIONS_MENU'     => true,
    'SHOW_GRID_SETTINGS_MENU'   => true,
    'SHOW_NAVIGATION_PANEL'     => true,
    'SHOW_PAGINATION'           => true,
    'SHOW_SELECTED_COUNTER'     => true,
    'SHOW_TOTAL_COUNTER'        => true,
    'SHOW_PAGESIZE'             => true,
    'SHOW_ACTION_PANEL'         => true,
    'ALLOW_COLUMNS_SORT'        => true,
    'ALLOW_COLUMNS_RESIZE'      => true,
    'ALLOW_HORIZONTAL_SCROLL'   => true,
    'ALLOW_SORT'                => true,
    'ALLOW_PIN_HEADER'          => true,
    'AJAX_OPTION_HISTORY'       => 'N'
    ]
);
?>