<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;
use Bitrix\Highloadblock\HighloadBlockTable;
// проверяем, установлен ли модуль "highloadblock"
if (!Loader::includeModule("highloadblock")) {
    throw new \Exception('Не загружены модули необходимые для работы модуля');
}

$arrHlblocks = array();
//получаем все HL-блоки
$arHlData = HighloadBlockTable::getList(array(
    'select' => array("ID", "NAME"), //Поля для выборки
    'order' => array('ID' => 'ASC'), //Сортировка
    'limit' => '50', // Кол-во выводимых записей
));
//формируем массив для Параметров компонента
while ($arHlbk = $arHlData->Fetch()) { //Формируем массив результатов
    $arrHlblocks[$arHlbk['ID']] = $arHlbk['NAME'];
}
// настройки компонента
$arComponentParameters = array(
// основной массив с параметрами
        'PARAMETERS' => array(
                //выбор HL-блока
                'IBLOCK_ID' => array(
                        'PARENT' => 'BASE',
                        'NAME' => Loc::getMessage("MESS_SET_HL_BLOCK") ,
                        'TYPE' => 'LIST',
                        'VALUES' => $arrHlblocks,
                        'REFRESH' => 'Y',
                        "DEFAULT" => "",
                ),
                //настройка "Выводить только активные"
                "UF_ADDRESS_IS_ACTIVE"  =>  array(
                    "PARENT"    =>  "BASE",
                    "NAME"      =>  Loc::getMessage('MESS_CHECKBOX_ACTIVE'),
                    "TYPE"      =>  "CHECKBOX",
                ),
                // настройки кэширования
                'CACHE_TIME' => array(
                        'DEFAULT' => 3600
                ),
        ),
);