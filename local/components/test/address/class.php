<?php
use \Bitrix\Main\Loader;
use \Bitrix\Highloadblock as HL;
use \Bitrix\Main\Engine\CurrentUser;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

class TestAddress extends CBitrixComponent{

    private function _checkModules() {
        if(!Loader::includeModule('highloadblock')){
            throw new \Exception('Не загружены модули необходимые для работы модуля test.address');
        }
        return true;
    }

    public function onPrepareComponentParams($arParams) {
        // тут пишем логику обработки параметров, дополнение параметрами по умолчанию
        return $arParams;
    }

    public function executeComponent() {
        $this -> _checkModules();
        //Проверяем, залогинен ли пользователь
        if(CurrentUser::get()->getId()){
            //формируем массив фильтра
            //Если параметр "Выводить только активные адреса" равен "Y"
            if($this -> arParams['UF_ADDRESS_IS_ACTIVE'] == "Y"){
                $arFilter["UF_ADDRESS_USER_ID"] = CurrentUser::get()->getId();
                $arFilter["UF_ADDRESS_IS_ACTIVE"] = true;
            }
            //если нет
            else{
                $arFilter["UF_ADDRESS_USER_ID"] = CurrentUser::get()->getId();
            }

            $hlblock = HL\HighloadBlockTable::getById($this -> arParams['IBLOCK_ID'])->fetch();

            $entity = HL\HighloadBlockTable::compileEntity($hlblock);
            $entity_data_class = $entity->getDataClass();
            //получаем список записей
            $rsData = $entity_data_class::getList(array(
               "select" => array("*"),
               "order" => array("ID" => "ASC"),
               "filter" => $arFilter  // Задаем параметры фильтра выборки
            ));
            //заполняем arResult записями
            while($arData = $rsData -> fetch()){
                $this -> arResult["ITEMS"][] = $arData;
            }
            //подключаем шаблон
            $this->includeComponentTemplate();
        }
    }
}
?>