<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$MESS["EXAMPLE_COMPONENT_PATH_ID"] = "local";
$MESS["EXAMPLE_COMPONENT_PATH_NAME"] = "Тестовые компоненты проекта";

$MESS["EXAMPLE_COMPSIMPLE_COMPONENT"] = 'Тестовый компонент "Адреса"';
$MESS["EXAMPLE_COMPONENT_DESCRIPTION"] = "Описание тестового компонента";

?>