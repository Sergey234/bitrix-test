<?php

$eventManager = \Bitrix\Main\EventManager::getInstance();
$eventManager -> addEventHandler('', 'AddresssOnAdd', 'OnAfterAddFunc');

function OnAfterAddFunc(Bitrix\Main\Entity\Event $event){
    
    //В документации написано, что при использовании типа кэша "Авто + Управляемое" кэш сбрасывается
    // автоматом, при изменении списка элементов
    
    $staticHtmlCache = \Bitrix\Main\Data\StaticHtmlCache::getInstance();
    $staticHtmlCache -> deleteAll();
}
?>
